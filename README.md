# README #

Small project using AngularJS representing a commercial website for a restaurant.

**Deployement:**

As the project is using rest web services, json-server was used for testing purposes.
 
In order to get it up and running, run a termina in the json-server folder and run this cmd :
 *json-server --watch db.json*. 

And then you can open *index.html* in the conFusion folder and get started.

That's it!